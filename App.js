import React from 'react'
import { Provider } from 'react-redux'
import Store from './store/configureStore'
import Navigation from './navigation/Navigation'

const App = () => {
  return (
    <Provider store={Store}>
      <Navigation/>
    </Provider>
  );
};

export default App;
