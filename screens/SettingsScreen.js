import React from 'react'
import { Button, Text, View } from 'react-native'
import { connect } from 'react-redux'
import * as PatientReducer from '../store/reducers/patientsReducer'
import * as PreferencesReducer from '../store/reducers/preferencesReducer'

const patientsList = [
  {name: 'Jean'},
  {name: 'Daniel'},
]

const prefs = {
  langue: 'FR',
  user: 'jean',
}

class SettingsScreen extends React.Component {

  componentDidUpdate() {
    console.log('UPDATING', this.props.patientsList)
    console.log('UPDATING', this.props.preferences)
  }

  setPatients() {
    const action = { type: PatientReducer.SET_ALL_PATIENTS, value: patientsList }
    this.props.dispatch(action)
  }

  setPreferences() {
    const action = {type: PreferencesReducer.SET_ALL_PREFERENCES, value: prefs }
    this.props.dispatch(action)
  }

  render() {
    console.log('PROPS', this.props)
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>Settings</Text>
        <Button
          onPress={() => this.setPatients()}
          title="Set Patient"
          color="#841584"
        />
        <Button
          onPress={() => this.setPreferences()}
          title="Set Preferences"
          color="#841584"
        />
      </View>
    )
  }
}
const mapStateToProps = (state) => {
  return {
    patientsList: state.patients.patientsList,
    preferences: state.preferences.preferences,
  }
}
export default connect(mapStateToProps)(SettingsScreen)
