import React from 'react'
import HomeScreen from "../screens/HomeScreen"
import SettingsScreen from "../screens/SettingsScreen"
import { createAppContainer, createBottomTabNavigator } from "react-navigation"
import Svg, { Path } from "react-native-svg"

const svgIcons = require('../assets/svg')

const TabNavigator = createBottomTabNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        title: 'Home',
      },
    },
    Settings: {
      screen: SettingsScreen,
      navigationOptions: {
        title: 'Settings',
      },
    },
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state
        let svgPath
        if (routeName === 'Home') {
          svgPath = svgIcons.home
        } else if (routeName === 'Settings') {
          svgPath = svgIcons.settings
        }
        return (
          <Svg height={32} width={32} viewBox={'0 0 24 24'}>
            <Path d={svgPath} fill={tintColor} />
          </Svg>
        )
      },
      tabBarOptions: {
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
      },
    })
  }
)

export default createAppContainer(TabNavigator)