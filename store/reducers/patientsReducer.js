const initialState = { patientsList: [] }

export const SET_ALL_PATIENTS = "SET_ALL_PATIENTS"
export const ADD_PATIENT = "ADD_PATIENT"
export const REMOVE_PATIENT = "REMOVE_PATIENT"

function patients(state = initialState, action) {
  console.log("PATIENT action type", action.type)
  console.log("PATIENT action value", action.value)
  let nextState
  switch (action.type) {
    case SET_ALL_PATIENTS:
      nextState = {
        ...state,
        patientsList: action.value
      }
      return nextState
    default:
      return state
  }
}

export default patients
