const initialState = { preferences: {} }

export const SET_ALL_PREFERENCES = "SET_ALL_PREFERENCES"

function patients(state = initialState, action) {
  console.log("PREFERENCES action type", action.type)
  console.log("PREFERENCES action value", action.value)
  let nextState
  switch (action.type) {
    case SET_ALL_PREFERENCES:
      nextState = {
        ...state,
        preferences: action.value
      }
      return nextState
    default:
      return state
  }
}

export default patients
