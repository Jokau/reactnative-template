import { combineReducers, createStore } from 'redux'
import patients from './reducers/patientsReducer'
import preferences from './reducers/preferencesReducer'

const reducers = combineReducers({
  patients,
  preferences,
})

export default createStore(reducers)
