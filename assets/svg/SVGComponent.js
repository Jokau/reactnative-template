import React from 'react'
import { Svg, Path } from 'react-native-svg'
import PropTypes from 'prop-types'

export default class SVGComponent extends React.Component {
  render() {
    <Svg viewBox="0 0 24 24" width={this.props.width} height={this.props.height}>
      <Path fill={this.props.color} d={this.props.path} />
    </Svg>
  }
}

SVGComponent.propTypes = {
  path: PropTypes.string.isRequired,
  width: PropTypes.func,
  height: PropTypes.string,
  color: PropTypes.string,
};

SVGComponent.defaultProps = {
  width: 24,
  height: 24,
  color: '#000000',
};


